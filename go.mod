module homework

go 1.14

require (
	github.com/gocarina/gocsv v0.0.0-20210408192840-02d7211d929d
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/namsral/flag v1.7.4-pre
	github.com/stretchr/testify v1.7.0 // indirect
)
